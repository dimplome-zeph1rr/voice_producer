FROM python:3.11

WORKDIR /app

RUN apt update -y

RUN apt install curl -y

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -

COPY pyproject.toml poetry.lock ./

RUN /root/.local/bin/poetry install --no-root --no-interaction --no-ansi --without dev

COPY voice_producer ./voice_producer

CMD ["/root/.local/bin/poetry", "run", "python", "-m", "voice_producer"]