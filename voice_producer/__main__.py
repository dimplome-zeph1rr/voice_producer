import uvicorn

from .app import app_name
from .settings import settings


def main():
    uvicorn.run(
        f"{app_name}.app:app",
        host=settings.server_host,
        port=settings.server_port,
        reload=settings.debug,
    )


if __name__ == "__main__":
    main()
