from uuid import UUID
from pydantic import BaseModel

class BaseResponse(BaseModel):
    status: bool
    data: UUID | bool | None