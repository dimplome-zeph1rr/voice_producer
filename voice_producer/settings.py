from pydantic import BaseSettings


class Settings(BaseSettings):
    server_host: str = "127.0.0.1"
    server_port: int = 8002
    
    debug: bool = True

    jwt_secret: str = "ChangeMe"
    algorithm: str = "HS256"

    log_level: str = "DEBUG"
    log_path: str = None

    recognizing_model: str = 'models/recognizing_model'
    speaker_model: str = 'models/speaker_model'


settings = Settings(
    _env_file=".env",
    _env_file_encoding="utf-8",
)
