import pathlib
import sys

from loguru import logger

from ..settings import settings

def configure_logger():
    logger.level("REQUEST", no=37, color="<yellow><bold>")
    logger.level("RESPONSE_SUCCESS", no=38, color="<green><bold>")
    logger.level("RESPONSE_ERROR", no=39, color="<red><bold>")

    logger_format = (
        "{time:YYYY-MM-DD HH:mm:ss.SSS!UTC} - [{level}] {message}"
    )

    logger.remove()
    logger.add(
        sys.stdout, level=settings.log_level, colorize=True, format=logger_format
    )

    if settings.log_path:
        logger.add(
            pathlib.Path(settings.log_path, "backend.log"),
            format=logger_format,
            level=settings.log_level,
            rotation="500mb",
            compression="gz",
        )
