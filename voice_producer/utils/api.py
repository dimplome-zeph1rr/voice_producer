import json
from time import time
from typing import Callable

from fastapi import Depends
from fastapi import Header
from fastapi import HTTPException
from fastapi import Request
from fastapi import Response
from fastapi import status
from fastapi.routing import APIRoute
from loguru import logger
from ..settings import settings


class LoggingRouteHandler(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def log_request(request: Request) -> Response:
            request_start_time = time()
            request_logger_data = {
                "method": request.method,
                "path": request.url.path,
                "host": request.client.host,
            }
            logger.log("REQUEST", json.dumps(request_logger_data))
            response = await original_route_handler(request)
            response_logger_data = {
                "type": "RESPONSE",
                "status_code": response.status_code,
                "request_time_duration": time() - request_start_time,
            }
            logger.log("RESPONSE_SUCCESS", json.dumps(response_logger_data))
            return response

        return log_request
