import os
from vosk import Model, KaldiRecognizer, SpkModel
from fastapi import HTTPException, status
from time import time
import numpy as np
import zipfile
import os
from io import BytesIO
import glob
import json
import math

from .settings import settings

def init_recognizer():
    model = Model(settings.recognizing_model)
    spk_model = SpkModel(settings.speaker_model)
    recognizer = KaldiRecognizer(model, 44100)
    recognizer.SetSpkModel(spk_model)
    return recognizer

class TmpFile:
    def __init__(self, file, filename) -> None:
        self.file = file
        self.tmp_filepath = os.path.join('tmp_data', filename)

    def __enter__(self):
        if not os.path.exists("tmp_data"):
            os.makedirs("tmp_data")
        with open(self.tmp_filepath, 'wb') as tmp_file:
            tmp_file.write(self.file)
        return self.tmp_filepath
    
    def __exit__(self, type, value, traceback):
        os.remove(self.tmp_filepath)

class VoiceService:
    def __init__(self):
        self.data_path = "data"
        if not os.path.exists("data"):
            os.makedirs("data")
        self.recognizer = init_recognizer()

    def check_file(self, filedata):
        data = self.recognize(filedata)
        if not data:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Невозможно разобрать текст записи"
            )
        if not data.get('spk'):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Невозможно разобрать голос"
            )
        return data

    def recognize(self, data) -> dict | None:
        if len(data) == 0:
            print("len")
            return False
        if self.recognizer.AcceptWaveform(data):
            result = json.loads(self.recognizer.Result())
            text = result.get('text')
            spk = result.get('spk', [])
            spk_frames = result.get('spk_frames', 0)

            return {"spk": spk, "spk_frames": spk_frames, 'message': text}
        return None
        
    def cosine_dist(self, x, y):
        nx = np.array(x)
        ny = np.array(y)
        return 1 - np.dot(nx, ny) / np.linalg.norm(nx) / np.linalg.norm(ny)

    def find_in_files(self, files, voice_spk):
        voice_spk = list(map(float, voice_spk))
        max_dist = 1
        result = None
        for file in files:
            with open(file) as filedata:
                spk = list(map(float, json.load(filedata)))
            dist = self.cosine_dist(voice_spk, spk)
            speaker_probability = math.ceil((1 - dist) * 100)
            print(speaker_probability)
            if dist < max_dist and speaker_probability > 65:
                max_dist = dist
                result = file.split(os.sep)[-1].split('_')[0]
        return result, max_dist
        
    async def find(self, file, user_id, filedata = None) -> list:
        filedata = await file.read()
        files = [f for f in glob.glob(os.path.join(self.data_path, f"{user_id}_*.spk"))]
        data = self.check_file(filedata).get('spk')
        result = self.find_in_files(files, data)
        if result[0] != user_id:
            return False
        return True
    
    
    async def find_all(self, file) -> set:
        filedata = await file.read()
        files = [
            os.path.join(self.data_path, path) for path in os.listdir(self.data_path)
        ]
        data = self.check_file(filedata).get('spk')
        result = self.find_in_files(files, data)
        return result[0]
    
    
    async def create(self, file, user_id):
        filedata = await file.read()
        data = self.check_file(filedata)
        with open(os.path.join("data", f"{user_id}_{time()}.spk"), 'w') as new_spk:
            json.dump(data.get('spk'), new_spk, ensure_ascii=False, indent=4)
        return True

