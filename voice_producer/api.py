from fastapi import APIRouter, Depends, UploadFile, HTTPException, status
from fastapi.responses import StreamingResponse
from typing import List
from uuid import UUID

from .utils.api import LoggingRouteHandler
from . import models
from .voice_service import VoiceService

router = APIRouter(route_class=LoggingRouteHandler)


@router.get('/files/{id}')
async def get_files(
    id: UUID
):
    raise HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST,
        detail='Невозможно получить голсовые файлы'
    )


@router.post('/find', response_model=models.BaseResponse)
async def verify_by_all_users(
    file: UploadFile,
    voice_service: VoiceService = Depends()
):
    result = await voice_service.find_all(file)
    return {"status": True, "data": result}


@router.post('/verify/{id}', response_model=models.BaseResponse)
async def verufy_user(
    id: UUID,
    file: UploadFile,
    voice_service: VoiceService = Depends()
):
    id = str(id)
    result = await voice_service.find(file, id)
    return {"status": True, "data": result}


@router.post('/{id}', response_model=models.BaseResponse)
async def add_picture(
    id: UUID,
    file: UploadFile,
    voice_service: VoiceService = Depends()
):
    id = str(id)
    result = await voice_service.create(file, id)
    return {"status": True, "data": result}