import json

from fastapi import FastAPI
from fastapi import HTTPException
from fastapi import Request
from fastapi.exception_handlers import request_validation_exception_handler
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from loguru import logger

from .api import router
from .utils.app import configure_logger

tags_metadata = []

app_name, app_version = "voice_producer", "0.1.0"

app = FastAPI(
    title=app_name,
    description="",
    version=app_version,
    openapi_tags=tags_metadata,
    openapi_url="/api/v1/openapi.json",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000", "http://magistratik.zeph1rr.ru:3000"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def startup_event():
    configure_logger()
    logger.debug(f"Application {app_name}:{app_version} successfully started")


@app.on_event("shutdown")
async def shutdown_event():
    logger.debug(f"Application {app_name}:{app_version} successfully stoped")


@app.exception_handler(HTTPException)
async def http_exception_handler(request: Request, exc: HTTPException):
    response_logger_data = {
        "status_code": exc.status_code,
        "error_class": "HTTPException",
        "detail": exc.detail,
    }
    logger.log("RESPONSE_ERROR", json.dumps(response_logger_data, ensure_ascii=False))
    return JSONResponse(
        content={"status": False, "detail": exc.detail}, status_code=exc.status_code, headers=exc.headers
    )


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    error_data = json.loads(exc.json().replace("\n", ""))[0]
    response_logger_data = {
        "status_code": 422,
        "error_class": "RequestValidationError",
        "detail": {"msg": error_data["msg"], "type": error_data["type"]},
    }
    logger.log("RESPONSE_ERROR", response_logger_data)
    return await request_validation_exception_handler(request, exc)


app.include_router(router=router, prefix="/api/v1")
